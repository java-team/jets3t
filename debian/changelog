jets3t (0.8.1+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Depend on libservlet-api-java instead of libservlet3.1-java
  * Removed the -java-doc package
  * Standards-Version updated to 4.7.0
  * Switch to debhelper level 13

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 28 Oct 2024 10:59:04 +0100

jets3t (0.8.1+dfsg-4) unstable; urgency=medium

  * Team upload.

  [ Miguel Landaeta ]
  * Remove myself from uploaders. (Closes: #871869)
  * Bump DH compat level to 10.
  * Update copyright info.
  * Simplify d/rules.

  [ Emmanuel Bourg ]
  * Set the source encoding to fix the build failure with recent JDKs
    (Closes: #893198)
  * Standards-Version updated to 4.1.4
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 07 Jun 2018 10:48:32 +0200

jets3t (0.8.1+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Transition to the Servlet API 3.1 (Closes: #801015)
  * Standards-Version updated to 3.9.8 (no changes)
  * Use secure Vcs-* URLs
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 21 Jun 2016 12:27:14 +0200

jets3t (0.8.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Fixed the version of java-xmlbuilder in the pom (Closes: #756487)
  * debian/control:
    - Standards-Version updated to 3.9.6 (no changes)
    - Use canonical URLs for the Vcs-* fields
  * debian/copyright:
    - Updated the Format URI
    - Fixed a wrong file path

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 02 Jul 2015 09:29:49 +0200

jets3t (0.8.1+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Drop xmlbuilder patch and add Depends and Build-Depends on
    libjava-xmlbuilder-java since it was finally packaged.
  * Bump Standards-Version to 3.9.2. No changes were required.
  * Add optional depends on java5-runtime for jets3t binary package.
  * Make copyright file DEP-5 compliant.
  * Refresh patches.
  * Update copyright file in order to apply Apache-2.0 and LGPL-2.1
    licenses correctly.

 -- Miguel Landaeta <miguel@miguel.cc>  Thu, 14 Apr 2011 20:33:04 -0430

jets3t (0.8.0+dfsg-1) unstable; urgency=low

  [ Miguel Landaeta ]
  * New upstream release.
  * Refresh patches.
  * Set target classes to Java 1.5. Since this release jets3t can not be
    built with Java 1.4 because the source code contains annotations.
  * Reorder dh7 helper invocation parameters.
  * Update copyright dates.
  * Add call to mh_clean in clean target.

  [ James Page ]
  * Enabled maven artifact deployment (Closes: #617670):
    - debian/control: Build-Depends added maven-repo-helper
    - debian/rules, debian/poms/jets3t.poms: install maven artifacts

 -- Miguel Landaeta <miguel@miguel.cc>  Thu, 10 Mar 2011 13:59:41 -0430

jets3t (0.7.4+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.1. No changes were required.
  * Replace Build-Depend on libservlet2.4-java with libservlet2.5-java.
  * Link javadoc to system javadocs.

 -- Miguel Landaeta <miguel@miguel.cc>  Fri, 30 Jul 2010 19:39:20 -0430

jets3t (0.7.3+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #573804),

 -- Miguel Landaeta <miguel@miguel.cc>  Wed, 25 Mar 2010 21:01:16 -0430
